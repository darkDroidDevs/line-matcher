#!/bin/bash
accuracy=0.8
fileArray=()
if  [ "$1" == "" ] || [ -z $1 ] > /dev/null
then echo "The Correct Syntax is: $0 [fileName] [accuracy\(0.1 - 1.0\)] or $0 [fileName]"
exit
fi

if [ -e $1 ] > /dev/null ; then
        echo > /dev/null
        else
        echo File does not exist.
        exit
fi

if [ "$2" != "" ];then
        if [[ $2 > 0 && $2 < 1 ]]; then
                accuracy=$2
        else
                echo $2 is not a valid number. Accuracy must be a number between 0 and 1.
                exit
        fi
fi

lines=$(wc -l $1 | sed 's/[^0-9]*//g')


#load file to memory
for (( line=1; line <= $lines; line++ ))
do
        percent=$(echo "$line*100/$lines"  |bc -l |xargs printf "%1.0f")
        echo -en "Loading file. . ." $percent/100%'          '\\r

        #echo "Line " $line " of " $lines

        fileArray[$line]=$(sed -n "$line p" $1)

done
echo -en \\n'Done. Now comparing Lines. . .'\\n
#echo "                                    "

for ((line= 1; line <=$lines; line++))
do
        echo "Line " $line " of " $lines
        #echo ${fileArray[$line]}
        lineString=$(echo "${fileArray[$line]}")
        numWords=$(echo $lineString | wc -w |sed 's/[^0-9]*//g')
        matchTrigger=$(echo "$numWords*$accuracy" |bc -l | xargs printf "%1.0f")
        echo 'Match trigger for this line is' $matchTrigger 'words'
        wordArray=( $lineString )
        if [[ "$line + 1" < $lines ]]; then

                for ((line2=$line; line2 <=$lines;line2++))
                do
                if [[ $line != $line2 ]]; then
                        echo -ne 'Comparing to line' $line2\\r
                        lineString2=$(echo $fileArray[$line2] | sed -n "$line2 p")
                        wordArray2=( $lineString2 )
                        numWords2=$(echo $lineString2 | wc -w)

                        matches=0
                        lineMatches=0
                        for (( i=0; i<$numWords; i++))
                        do
                                for ((i2=0; i2<$numWords2; i2++))
                                do
                                        if [[ $(echo "${wordArray[$i]}") == $(echo "${wordArray[$i2]}") ]]; then
                                                ((matches++))
                                                if [[ $matches -ge $matchTrigger ]]; then
                                                break
                                                fi

                                        fi
                                done

                                if [[ $matches -ge $matchTrigger ]]; then
                                        ((lineMatches++))
                                        if [[ $lineMatches == 1 ]]; then
                                                echo -ne \n
                                                echo Match Found!
                                                echo -en \\007
                                                $(echo "Matches found for line "$line": "$lineString ':
                                                ' $line2':' $lineString2 '
                                                ' >> results.txt)
                                        else
                                                $(echo $line2": "$lineString2  '
                                                ' >> results.txt)
                                        fi
                                fi
                                matches=0

                        done
                        echo -ne 'done                              '\\r

                fi


                done
        fi


done
echo 'done'
echo -en \\007 \\007 \\007